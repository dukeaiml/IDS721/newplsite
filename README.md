# Week 1 Mini Project - Zola portfolio site

I used Zola (the SSG - static site generator), Tera (the template engine) and Zplit (Zola's provided theme) to set up my personal site. It's not yet complete but will be rolling out gradually. :)
<br><br>
Feel free to click [here](https://newplsite-dukeaiml-ids721-ee2009fc7e87cf675d68896159792921beca9.gitlab.io) to visit!


## Requirements
- [x] Static site generated with Zola 
- [x] Home page and portfolio project page templates (Theme in use: Zplit [Zola](https://www.getzola.org/themes/zplit/)/[Github](https://github.com/gicrisf/zplit))
- [x] Styled with CSS
- [x] GitLab repo with source code

## Screenshots
![Home Page](screenshots/home.png)

![Project Section](screenshots/ProjectSection.png)

![Music Section](screenshots/MusicSection.png)

![Duke CME](screenshots/DukeCME.png)
