+++
title="🪕 Duke Chinese Music Ensemble"
date=2023-12-01
+++

The first thing I was wondering about Duke is whether it has a Chinese Ensemble. Hopefully, there is one, and it even has a director.
<br><br>
Check out my [first concert](https://www.youtube.com/watch?v=_hMmPAJrHMo&t=3081s)(:style="color: white;") in Duke CME.
<br><br>
Our upcoming performance: DCSSA's Chinese New Year's Gala 🎉 on 02/24 at Page Auditorium 😃. Come join us!
<br><br>
And also! Look out to our next concert on 04/21 at Baldwin Auditorium. ;)